class TweetsController < ApplicationController
  def index
    # page_quantity = 10
    # offset = page_num * page_quantity

    # tweet = Tweet.order(created_at: :desc).limit(:page_quantity).offset(offset)

    # render json: tweet

    tweets = Tweet.by_user(search_params[:user_id])
    render json: tweets.all
  end

  private
  def search_params
    params.permit(:user_id)
  end
end
